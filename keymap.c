// -*- compile-command: "qmk compile -kb annepro2/c18 -km karloguidoni" -*-
 /* Copyright 2021 OpenAnnePro community
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

#include QMK_KEYBOARD_H
#include "keymap_brazilian_abnt2.h"
#include "sendstring_brazilian_abnt2.h"

#include "features/custom_shift_keys.h"
#include "features/autocorrection.h"

#include "features/unicodemap.h"



enum anne_pro_layers {
    BASE,
    FN1,
    FN2,
    NAV,
    SYM,
    MOUSE,
    /* COLEMAK */
};

enum custom_keycodes {
    R_ASSIGN = SAFE_RANGE,
};


/* BEG: */
/* FROM: https://gist.githubusercontent.com/frederik-h/578524281a0b63ab143de1acd1d30bee/raw/f2a6a223275c6a1de59ae0fae2b69e2513c7e62f/extended_space_cadet_shift.c */
typedef struct {
  bool is_press_action;
  int state;
} tap;

enum {
  SINGLE_HOLD     = 1,
  SINGLE_TAP      = 2,
  SINGLE_TAP_HOLD = 3,
  DOUBLE_TAP      = 4,
  DOUBLE_TAP_HOLD = 5,
  TRIPLE_TAP      = 6,
  TRIPLE_TAP_HOLD = 7,
  QUAD_TAP        = 8,
};


enum {
  QUAD_TAP_DANCE_LEFT_SHIFT = 0,
  QUAD_TAP_DANCE_RIGHT_SHIFT = 1,
  SOME_OTHER_DANCE
};


int cur_dance (qk_tap_dance_state_t *state) {
  if(state->pressed)
    return SINGLE_HOLD;

  if (state->count == 1) {
      if(state->pressed) return SINGLE_TAP_HOLD;
      else return SINGLE_TAP;
  }

  if (state->count == 2) {
      if (state->pressed) return DOUBLE_TAP_HOLD;
      else return DOUBLE_TAP;
  }

  if (state->count == 3) {
      if (state->pressed) return TRIPLE_TAP_HOLD;
      else return TRIPLE_TAP;
  }

  if (state->count == 4)
    return QUAD_TAP;

   return -1;
}

static int quad_tap_dance_left_shift_tap_state = 0;
static int quad_tap_dance_right_shift_tap_state = 0;

void quad_tap_dance_left_shift_finished (qk_tap_dance_state_t *state, void *user_data) {
  quad_tap_dance_left_shift_tap_state = cur_dance(state);
  switch (quad_tap_dance_left_shift_tap_state) {
  case SINGLE_TAP: // send (
    register_mods(MOD_BIT(KC_LSFT));
    register_code(KC_9);
    break;
  case DOUBLE_TAP: // send [
    unregister_code(KC_9);
    register_code(KC_LBRC);
    break;
  case TRIPLE_TAP: // send {
    unregister_code(KC_9);
    register_mods(MOD_BIT(KC_LSFT));
    register_code(KC_LBRC);
    break;
  case QUAD_TAP:   // send <
    unregister_code(KC_9);
    register_mods(MOD_BIT(KC_LSFT));
    register_code(KC_COMM);
    break;
  case SINGLE_HOLD:       // normal left shift
    unregister_code(KC_9);
    register_mods(MOD_BIT(KC_LSFT));
  }
}

void quad_tap_dance_left_shift_reset (qk_tap_dance_state_t *state, void *user_data) {
  switch (quad_tap_dance_left_shift_tap_state) {
  case SINGLE_TAP:
    unregister_code(KC_9);
    unregister_mods(MOD_BIT(KC_LSFT));
    break;
  case DOUBLE_TAP:
    unregister_code(KC_LBRC);
    break;
  case TRIPLE_TAP:
    unregister_code(KC_LBRC);
    unregister_mods(MOD_BIT(KC_LSFT));
    break;
  case QUAD_TAP:
    unregister_code(KC_COMM);
    unregister_mods(MOD_BIT(KC_LSFT));
    break;
  case SINGLE_HOLD:
    unregister_mods(MOD_BIT(KC_LSFT));
    break;
  }
  quad_tap_dance_left_shift_tap_state = 0;
}


void quad_tap_dance_right_shift_finished (qk_tap_dance_state_t *state, void *user_data) {
  quad_tap_dance_right_shift_tap_state = cur_dance(state);
  switch (quad_tap_dance_right_shift_tap_state) {
  case SINGLE_TAP: // send )
    register_mods(MOD_BIT(KC_RSFT));
    register_code(KC_0);
    break;
  case DOUBLE_TAP: // send ]
    unregister_code(KC_0);
    register_code(KC_RBRC);
    break;
  case TRIPLE_TAP: // send }
    unregister_code(KC_0);
    register_mods(MOD_BIT(KC_RSFT));
    register_code(KC_RBRC);
    break;
  case QUAD_TAP:   // send >
    unregister_code(KC_0);
    register_mods(MOD_BIT(KC_RSFT));
    register_code(KC_DOT);
    break;
  case SINGLE_HOLD:       // normal right shift
    unregister_code(KC_0);
    register_mods(MOD_BIT(KC_RSFT));
  }
}

void quad_tap_dance_right_shift_reset (qk_tap_dance_state_t *state, void *user_data) {
  switch (quad_tap_dance_right_shift_tap_state) {
  case SINGLE_TAP:
    unregister_code(KC_0);
    unregister_mods(MOD_BIT(KC_RSFT));
    break;
  case DOUBLE_TAP:
    unregister_code(KC_RBRC);
    break;
  case TRIPLE_TAP:
    unregister_code(KC_RBRC);
    unregister_mods(MOD_BIT(KC_RSFT));
    break;
  case QUAD_TAP:
    unregister_code(KC_DOT);
    unregister_mods(MOD_BIT(KC_RSFT));
    break;
  case SINGLE_HOLD:
    unregister_mods(MOD_BIT(KC_RSFT));
    break;
  }
  quad_tap_dance_right_shift_tap_state = 0;
}

qk_tap_dance_action_t tap_dance_actions[] = {
  [QUAD_TAP_DANCE_LEFT_SHIFT]  = ACTION_TAP_DANCE_FN_ADVANCED(NULL, quad_tap_dance_left_shift_finished, quad_tap_dance_left_shift_reset),
  [QUAD_TAP_DANCE_RIGHT_SHIFT] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, quad_tap_dance_right_shift_finished, quad_tap_dance_right_shift_reset),

};
/* END: */

#define MOUSEKEY_INTERVAL 16
#define MOUSEKEY_DELAY 0
#define MOUSEKEY_TIME_TO_MAX 60
#define MOUSEKEY_MAX_SPEED 7
#define MOUSEKEY_WHEEL_DELAY 0

#define TAB_GUI  LGUI_T(KC_TAB)
#define CTL_ESC  CTL_T(KC_ESC)
#define CTL_ENT  CTL_T(KC_ENT)
#define CTL_DOT  CTL_T(KC_DOT)
#define CTL_BS   CTL_T(KC_BSPC)
#define LS_TD    TD(QUAD_TAP_DANCE_LEFT_SHIFT)
#define RS_TD    TD(QUAD_TAP_DANCE_RIGHT_SHIFT)
#define SL_GUI   RGUI_T(KC_SLSH)
#define NAV_SYML LT(NAV, KC_ENT)
#define NAV_SYMR LT(NAV, KC_SPC)
#define NUMB_PAD LT(FN1, KC_ENT)
#define LALT_BKP LT(KC_LALT, KC_BSPC)
#define FUN_KEYS MO(FN2)
#define MOUSE_L  MO(MOUSE)

/* #define CLMK     TG(COLEMAK) */

#define SYM_PAD  OSL(SYM)

#define DED_ACT  ALGR(KC_QUOT)
#define DED_CRS  ALGR(KC_GRV)
#define DED_TLD  ALGR(S(KC_GRV))
#define DED_CIR  ALGR(KC_6)

#define LSQBKT   KC_LBRC
#define RSQBKT   KC_RBRC
#define LSQCKT   S(KC_LBRC)
#define RSQCKT   S(KC_RBRC)

// Define R related REPL code execution in Emacs
#define SFT_ENT S(KC_ENT)     // send Shift+Enter
#define CRT_ENT C(KC_ENT)     // send Control+Enter
#define CRT_BSL C(KC_BSLS)    // send Control+Backslash
#define CST_BSL RCS(KC_BSLS)  // send Control+Shift+Backslash

// clang-format off
// Key symbols are based on QMK. Use them to remap your keyboard

 const uint16_t keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/*

[BASE] = LAYOUT_60_ansi(
  __ESC__ , ___1___ , ___2___ , ___3___ , ___4___ , ___5___ , ___6___ , ___7___ , ___8___ , ___9___ , ___0___ , ___-___ , ___+___ ,  __BSP__ ,
  __TAB__ , ___Q___ , ___W___ , ___E___ , ___R___ , ___T___ , ___Y___ , ___U___ , ___I___ , ___O___ , ___P___ , ___[___ , ___]___ ,  ___\___ ,
  _CAPS__ , ___A___ , ___S___ , ___D___ , ___F___ , ___G___ , ___H___ , ___J___ , ___K___ , ___L___ , ___;___ , ___'___ , _ENTER_ ,
  _LSFT__ , ___Z___ , ___X___ , ___C___ , ___V___ , ___B___ , ___N___ , ___M___ , ___,___ , ___.___ , ___/___ , _RSFT__ ,
  _LCTL__ , __GUI__ , _LALT__ ,                          __SPC__ ,                          _RALT__ , __FN1__ , __FN2__ , _RCTL__
),

*/

[BASE] = LAYOUT_60_ansi(
  TAB_GUI , KC_Q    , KC_W    , KC_E    , KC_R    , KC_T    , KC_Y    , KC_U    , KC_I    , KC_O    , KC_P    , _______ , KC_BSPC , _______  ,
  CTL_ESC , KC_A    , KC_S    , KC_D    , KC_F    , KC_G    , KC_H    , KC_J    , KC_K    , KC_L    , KC_COLN , KC_DQT  , _______ , _______  ,
  LS_TD   , KC_Z    , KC_X    , KC_C    , KC_V    , KC_B    , KC_N    , KC_M    , KC_COMM , CTL_DOT , SL_GUI  , RS_TD   , _______ ,
  _______ , _______ , KC_LALT , CTL_BS  , NAV_SYML, _______ , NAV_SYMR, NUMB_PAD, KC_RALT , _______ , _______ , _______ ,
  _______ , _______ , _______ ,                          _______ ,                          _______ , _______ , _______ , _______
),

/*
[COLEMAK] = LAYOUT_60_ansi(
  TAB_GUI , KC_Q    , KC_W    , KC_F    , KC_P    , KC_G    , KC_J    , KC_L    , KC_U    , KC_Y    , KC_COLN , _______ , KC_BSPC , _______  ,
  CTL_ESC , KC_A    , KC_R    , KC_S    , KC_T    , KC_D    , KC_H    , KC_N    , KC_E    , KC_I    , KC_O    , KC_DQT  , _______ , _______  ,
  LS_TD   , KC_Z    , KC_X    , KC_C    , KC_V    , KC_B    , KC_K    , KC_M    , KC_COMM , CTL_DOT , SL_GUI  , RS_TD   , _______ ,
  _______ , _______ , KC_LALT , CTL_BS  , NAV_SYML, _______ , NAV_SYMR, NUMB_PAD, KC_RALT , _______ , _______ , _______ ,
  KC_ESC  , _______ , _______ ,                          _______ ,                          _______ , _______ , _______ , _______
),
*/

[FN1] = LAYOUT_60_ansi(
  KC_BSPC , KC_PAST , KC_7    , KC_8    , KC_9    , KC_MINS , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ ,
  KC_BSPC , KC_SLSH , KC_4    , KC_5    , KC_6    , KC_PPLS , _______ , KC_LCTL , KC_LSFT , KC_LALT , KC_LGUI , _______ , KC_ENT  , _______ ,
  _______ , KC_0    , KC_1    , KC_2    , KC_3    , KC_DOT  , _______ , _______ , _______ , _______ , _______ , _______ , _______ ,
  _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ ,
  _______ , _______ , _______ ,                     _______ ,                               _______ , _______ , _______ , _______
),

[FN2] = LAYOUT_60_ansi(
  _______ , KC_F12  , KC_F7   , KC_F8   , KC_F9   , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ ,
  _______ , KC_F11  , KC_F4   , KC_F5   , KC_F6   , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ ,
  _______ , KC_F10  , KC_F1   , KC_F2   , KC_F3   , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ ,
  _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ ,
  _______ , _______ , _______ ,                          _______ ,                          _______ , _______ , _______ , _______
),

[NAV] = LAYOUT_60_ansi(
  KC_GRV  , KC_EXLM , KC_AT   , KC_HASH , KC_DLR  , KC_PERC , KC_CIRC , KC_UNDS , KC_MINS , KC_EQL  , KC_PPLS , _______ , KC_0    , _______ ,
  _______ , KC_AMPR , KC_ASTR , KC_BSLS , KC_PIPE , R_ASSIGN, KC_LEFT , KC_DOWN , KC_UP   , KC_RGHT , FUN_KEYS, _______ , _______ , _______ ,
  _______ , LSQBKT  , RSQBKT  , LSQCKT  , RSQCKT  , KC_GRV  , CRT_ENT , SFT_ENT , CRT_BSL , CST_BSL , _______ , _______ , _______ ,
  _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ ,
  _______ , _______ , _______ ,                          _______ ,                          _______ , _______ , _______ , _______
),

/*
[SYM] = LAYOUT_60_ansi(
  _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ , _______ ,  _______ ,
  _______ , KC_EXLM , KC_AT   , KC_HASH , KC_DLR  , KC_PERC , KC_CIRC , KC_UNDS , KC_MINS , KC_EQL  , KC_PPLS , _______ , _______ ,  _______ ,
  _______ , X(DEA) , KC_ASTR , KC_BSLS , KC_PIPE , _______ , KC_LEFT , KC_DOWN , KC_UP   , KC_RGHT , _______ , _______ , _______ ,
  _______ , _______ , _______ , _______ , _______ , KC_GRV  , KC_HOME , KC_PGDN , KC_PGUP , KC_END  , _______ , _______ ,
  _______ , _______ , _______ ,                          _______ ,                          _______ , _______ , _______ , KC_PSCR
),
*/

/*
[MOUSE] = LAYOUT_60_ansi(
  _______ , _______ , _______ , _______ , _______ , _______ , KC_WH_L , KC_WH_D , KC_WH_U , KC_WH_R , _______ , _______ , _______ , _______ ,
  _______ , _______ , KC_ACL0 , KC_ACL1 , KC_ACL2 , _______ , KC_MS_L , KC_MS_D , KC_MS_U , KC_MS_R , KC_BTN2 , _______ , _______ , _______ ,
  _______ , _______ , _______ , _______ , _______ , _______ , KC_BTN3 , KC_BTN4 , KC_BTN5 , _______ , _______ , _______ , _______ ,
  _______ , _______ , _______ , _______ , _______ , _______ , KC_BTN1 , _______ , _______ , _______ , _______ , _______ ,
  _______ , _______ , _______ ,                          _______ ,                          _______ , _______ , _______ , _______
 ),
*/

};

/*
  BEG:
  FROM: https://getreuer.info/posts/keyboards/custom-shift-keys/index.html
*/

const custom_shift_key_t custom_shift_keys[] = {
  {KC_DQT,  KC_QUOT }, // Shift " is '
  {KC_COLN, KC_SCLN}, // Shift : is ;
};

uint8_t NUM_CUSTOM_SHIFT_KEYS =
    sizeof(custom_shift_keys) / sizeof(custom_shift_key_t);

/*
  END:
*/

enum combo_events {
  // . and C => activate Caps Word.
  CAPS_COMBO,
  // // d and f => send (
  // PARLEFT_COMBO,
  // // j and k => send )
  // PARRIGHT_COMBO,
  // // e and r => send [
  // LBRC_COMBO,
  // // u and i => send ]
  // RBRC_COMBO,
  // // c and v => send {
  // LCBRC_COMBO,
  // // m and , => send }
  // RCBRC_COMBO,
  // z and x => copy text
  /* COPY_COMBO, */
  // c and v => paste text
  /* PASTE_COMBO, */
  // d and f => send contro+f
  /* CTRL_F_COMBO, */
  // e and r => send ctrl+tab
  /* CTRL_TAB_COMBO, */
  // q and w => send ctrl+shift+tab
  /* CTRL_SHIFT_TAB_COMBO, */
  // c and , => types a period, space, and sets one-shot mod for shift.
  // This combo is useful to flow between sentences.
  /* END_SENTENCE_COMBO, */
  COMBO_LENGTH
};
uint16_t COMBO_LEN = COMBO_LENGTH;

const uint16_t caps_combo[] PROGMEM = {KC_COMM, KC_C, COMBO_END};
// const uint16_t parleft_combo[] PROGMEM = {KC_D, KC_F, COMBO_END};
// const uint16_t parright_combo[] PROGMEM = {KC_J, KC_K, COMBO_END};
// const uint16_t lbrc_combo[] PROGMEM = {KC_E, KC_R, COMBO_END};
// const uint16_t rbrc_combo[] PROGMEM = {KC_U, KC_I, COMBO_END};
// const uint16_t lcbrc_combo[] PROGMEM = {KC_C, KC_V, COMBO_END};
// const uint16_t rcbrc_combo[] PROGMEM = {KC_M, KC_COMM, COMBO_END};
/* const uint16_t copy_combo[] PROGMEM = {KC_Z, KC_X, COMBO_END}; */
/* const uint16_t paste_combo[] PROGMEM = {KC_C, KC_V, COMBO_END}; */
/* const uint16_t ctrl_f_combo[] PROGMEM = {KC_D, KC_F, COMBO_END}; */
/* const uint16_t ctrl_tab_combo[] PROGMEM = {KC_E, KC_R, COMBO_END}; */
/* const uint16_t ctrl_shift_tab_combo[] PROGMEM = {KC_Q, KC_W, COMBO_END}; */
/* const uint16_t end_sentence_combo[] PROGMEM = {KC_I, KC_O, COMBO_END}; */

combo_t key_combos[] = {
  [CAPS_COMBO] = COMBO_ACTION(caps_combo),
  //  [PARLEFT_COMBO] = COMBO_ACTION(parleft_combo),
  //  [PARRIGHT_COMBO] = COMBO_ACTION(parright_combo),
  //  [LBRC_COMBO] = COMBO_ACTION(lbrc_combo),
  //  [RBRC_COMBO] = COMBO_ACTION(rbrc_combo),
  //  [LCBRC_COMBO] = COMBO_ACTION(lcbrc_combo),
  //  [RCBRC_COMBO] = COMBO_ACTION(rcbrc_combo),
  /* [COPY_COMBO] = COMBO_ACTION(copy_combo), */
  /* [PASTE_COMBO] = COMBO_ACTION(paste_combo), */
  /* [CTRL_F_COMBO] = COMBO_ACTION(ctrl_f_combo), */
  /* [CTRL_TAB_COMBO] = COMBO_ACTION(ctrl_tab_combo), */
  /* [CTRL_SHIFT_TAB_COMBO] = COMBO_ACTION(ctrl_shift_tab_combo), */
  /* [END_SENTENCE_COMBO] = COMBO_ACTION(end_sentence_combo), */
};

void process_combo_event(uint16_t combo_index, bool pressed) {
  if (pressed) {
    switch(combo_index) {

      case CAPS_COMBO:
        caps_word_on();  // Activate Caps Word.
        break;

        // case PARLEFT_COMBO:
        //   tap_code16(LSFT(KC_9));
        //   break;

        // case PARRIGHT_COMBO:
        //   tap_code16(LSFT(KC_0));
        //   break;

        // case LBRC_COMBO:
        //   tap_code16(KC_LBRC);
        //   break;

        // case RBRC_COMBO:
        //   tap_code16(KC_RBRC);
        //   break;

        // case LCBRC_COMBO:
        //   tap_code16(LSFT(KC_LBRC));
        //   break;

        // case RCBRC_COMBO:
        //   tap_code16(LSFT(KC_RBRC));
        //   break;

      /* case COPY_COMBO: */
      /*   tap_code16(KC_COPY); */
      /*   break; */

      /* case PASTE_COMBO: */
      /*   tap_code16(KC_PASTE); */
      /*   break; */

      /* case CTRL_F_COMBO: */
      /*   tap_code16(C(KC_F)); */
      /*   break; */

      /* case CTRL_TAB_COMBO: */
      /*   tap_code16(C(KC_TAB)); */
      /*   break; */

      /* case CTRL_SHIFT_TAB_COMBO: */
      /*   tap_code16(C(S(KC_TAB))); */
      /*   break; */

      /* case END_SENTENCE_COMBO: */
      /*   SEND_STRING(". "); */
      /*   add_oneshot_mods(MOD_BIT(KC_LSFT));  // Set one-shot mod for shift. */
      /*   break; */

    }
  }
}

bool process_record_user(uint16_t keycode, keyrecord_t* record) {
    if (!process_custom_shift_keys(keycode, record)) { return false; }
    if (!process_autocorrection(keycode, record)) { return false; }
    switch (keycode) {
    case R_ASSIGN:
        if (record->event.pressed) {
            // when keycode R_ASSIGN is pressed
            SEND_STRING(" <-");
        } else {
            // when keycode R_ASSIGN is released
        }
        break;
    }
    return true;
}
