# Custom RGB matrix handling
RGB_MATRIX_ENABLE = yes
RGB_MATRIX_DRIVER = custom
TAP_DANCE_ENABLE = yes
UNICODE_ENABLE = no        # Unicode (can't be used with unicodemap)
UNICODEMAP_ENABLE = yes      # Enable extended unicode
EXTRAKEY_ENABLE = yes
COMMAND_ENABLE = no
COMBO_ENABLE = yes
MOUSEKEY_ENABLE = yes
LTO_ENABLE = yes
CAPS_WORD_ENABLE = yes
SRC += features/custom_shift_keys.c
SRC += features/autocorrection.c
