# Anne Pro 2 QMK config

Highly opinated QMK config, including keycodes for ESS-mode in Emacs and DWM Window Manager. This layout is inspirated in [Miryoku](https://github.com/manna-harbour/miryoku) layout.

The keyboard layout can be visualized [here](http://www.keyboard-layout-editor.com/##@_name=Anne%20Pro%202%20(inspirated%20by%20https%2F:%2F%2F%2F%2Fgithub.com%2F%2Fmanna-harbour%2F%2Fmiryoku)&author=Karlo%20Guidoni%20%2F@kguidonimartins&notes=The%20keymaps%20for%20this%20keyboard%20lives%20here%2F:%20https%2F:%2F%2F%2F%2Fgitlab.com%2F%2Fkguidonimartins%2F%2Fqmk-keymap-annepro2%3B&@=Super%0ATab&=Q&=W&=E&=R&=T&=Y&=U&=I&=O&=P&_x:1%3B&=BcSp%3B&@_w:1.5%3B&=Control%0AEsc&=A&=S&=D&=F&=G&=H&=J&=K&=L&=%2F:&=%22%3B&@_w:1.75%3B&=Shift&=Z&=X&=C&=V&=B&=N&=M&=,&=Control%0A.&=Super%0A%2F%2F&=Shift%3B&@_x:3.25%3B&=Alt&=BcSp%0AControl&=NAV%0AEnter&_x:1%3B&=NAV%0ASpace&=NUM&=Alt).
